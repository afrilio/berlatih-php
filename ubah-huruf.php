<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
function ubah_huruf($string){
$arrhuruf = ['a','b','c','d','e','f','g','h','i','j','k','l','m',
             'n','o','p','q','r','s','t','u','v','w','x','y','z'];
$hurufext = "";
 if(!is_string($string)){
     return "a";
 }
 else{
     foreach (str_split ($string) as $str) {
         for ($i=0; $i < count($arrhuruf) ; $i++) { 
             if ($str == $arrhuruf[$i]){ //tambah strtolower/strtoupper utk jadi huruf besar/kecil
                 $hurufext .= $arrhuruf[$i+1];
             }
         }
     }
     return $hurufext . "<br>";
 }
//kode di sini
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>    
</body>
</html>